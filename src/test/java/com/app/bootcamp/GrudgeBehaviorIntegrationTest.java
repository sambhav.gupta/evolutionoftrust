package com.app.bootcamp;

import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class GrudgeBehaviorIntegrationTest {
    @Test
    public void shouldDisplayCooperateScoreForAllCooperate() {
        GrudgeBehavior grudgeBehavior = new GrudgeBehavior("CopyCatPlayer");
        Player player = new Player(grudgeBehavior, "Player1");

        Machine machine = new Machine();

        CopyCatBehavior copyCatBehavior2 = new CopyCatBehavior(player.getName());
        Player copyCatPlayer = new Player(copyCatBehavior2, "CopyCatPlayer");
        PrintStream out = mock(PrintStream.class);
        Game game = new Game(player, copyCatPlayer, machine, 5, out);
        game.addObserver(grudgeBehavior);
        game.addObserver(copyCatBehavior2);

        game.start();

        verify(out).println( "2 2");
        verify(out).println( "4 4");
        verify(out).println( "6 6");
        verify(out).println( "8 8");
        verify(out).println( "10 10");
    }

    @Test
    public void shouldDisplayGrudgeBehaviorIfOpponentCheats() {
        GrudgeBehavior grudgeBehavior = new GrudgeBehavior("AlwaysCheat");
        Player player = new Player(grudgeBehavior, "Player1");

        Machine machine = new Machine();
        Player alwaysCheatPlayer = new Player(new CheatPlayerMoveBehavior(), "AlwaysCheat");
        PrintStream out = mock(PrintStream.class);
        Game game = new Game(player, alwaysCheatPlayer, machine, 5, out);
        game.addObserver(grudgeBehavior);

        game.start();

        verify(out, times(5)).println( "-1 3");



    }
}
