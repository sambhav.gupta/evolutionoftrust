package com.app.bootcamp;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GrudgerBehaviorTest {

    @Test
    public void shouldReturnCooperateForFirstMove() {
        Player player = new Player(new GrudgeBehavior("CopyCatPlayer"), "Player1");
        Moves expectedMove = Moves.COOPERATE;
        Moves actualMove = player.makeMove();
        assertEquals(expectedMove,actualMove);
    }

    @Test
    public void shouldReturnCheatIfAPlayerCheats() {
        GrudgeBehavior grudgeBehavior = new GrudgeBehavior("CopyCatPlayer");
        Player player = new Player(grudgeBehavior, "Player1");
        Moves expectedMove = Moves.CHEAT;
        Notification notification = new Notification("CopyCatPlayer", Moves.CHEAT);
        grudgeBehavior.update(null, notification);
        Moves actualMove = player.makeMove();
        assertEquals(expectedMove,actualMove);
    }

    @Test
    public void shouldReturnCheatIfAPlayerCheatsAndThenCooperates() {
        GrudgeBehavior grudgeBehavior = new GrudgeBehavior("CopyCatPlayer");
        Player player = new Player(grudgeBehavior, "Player1");
        Moves expectedMove = Moves.CHEAT;

        Notification notification = new Notification("CopyCatPlayer", Moves.CHEAT);
        grudgeBehavior.update(null, notification);

        notification = new Notification("CopyCatPlayer", Moves.COOPERATE);
        grudgeBehavior.update(null, notification);
        Moves actualMove = player.makeMove();
        assertEquals(expectedMove,actualMove);
    }

    @Test
    public void shouldReturnCooperateOnSelfNotification() {
        GrudgeBehavior grudgeBehavior = new GrudgeBehavior("CopyCatPlayer");
        Player player = new Player(grudgeBehavior, "Player1");
        Notification notification = new Notification("Player1", Moves.CHEAT);
        grudgeBehavior.update(null, notification);

        Moves actualMove = player.makeMove();
        Moves expectedMove = Moves.COOPERATE;
        assertEquals(expectedMove, actualMove);
    }


}
