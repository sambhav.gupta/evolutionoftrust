package com.app.bootcamp;

import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class GameIntegrationTest {

    @Test
    public void shouldMachineReturnCorrectScoresForCheatAndCooperatePlayerOneRound() {

        PlayerMoveBehavior playerAlwaysCooperateBehavior = new CooperatePlayerMoveBehavior();
        PlayerMoveBehavior playerAlwaysCheatBehavior = new CheatPlayerMoveBehavior();

        Player player1 = new Player(playerAlwaysCooperateBehavior, "Player1");
        Player player2 = new Player(playerAlwaysCheatBehavior, "Player1");
        Machine machine = new Machine();
        int rounds = 5;

        PrintStream out = mock(PrintStream.class);

        new Game(player1, player2, machine, rounds, out).start();


        verify(out).println( "-1 3");
        verify(out).println( "-2 6");
        verify(out).println( "-3 9");
        verify(out).println( "-4 12");
        verify(out).println( "-5 15");

    }

    @Test
    public void shouldMachineReturnCorrectScoresForCopyCatAndDetectivePlayerFiveRounds() {

        CopyCatBehavior player1CopyCatBehavior = new CopyCatBehavior("p2");
        DetectiveBehaviour player2DetectiveBehavior = new DetectiveBehaviour("p1");

        Player player1 = new Player(player1CopyCatBehavior, "p1");
        Player player2 = new Player(player2DetectiveBehavior, "p2");
        Machine machine = new Machine();
        int rounds = 5;

        PrintStream out = mock(PrintStream.class);

        Game game = new Game(player1, player2, machine, rounds, out);
        game.addObserver(player1CopyCatBehavior);
        game.addObserver(player2DetectiveBehavior);
        game.start();


        verify(out).println( "2 2");
        verify(out).println( "1 5");
        verify(out).println( "4 4");
        verify(out).println( "6 6");
        verify(out).println( "5 9");

    }
}
