package com.app.bootcamp;

import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.*;


public class GameTest {


    @Test
    public void shouldMachineReturnCorrectScores() {
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        int rounds = 1;

        when(player1.makeMove()).thenReturn(Moves.CHEAT);
        when(player2.makeMove()).thenReturn(Moves.COOPERATE);
        PrintStream out = mock(PrintStream.class);
        Score score = new Score(34, 22);
        when(machine.calculateScore(Moves.CHEAT, Moves.COOPERATE)).thenReturn(score);


        new Game(player1, player2, machine, rounds, out).start();


        verify(player1).makeMove();
        verify(player2).makeMove();

        verify(machine).calculateScore(Moves.CHEAT, Moves.COOPERATE);

        verify(out).println(score.getPlayer1Score() + " " + score.getPlayer2Score());

    }

    @Test
    public void shouldMachineReturnCorrectScoresForMultipleRounds() {
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        int rounds = 2;

        when(player1.makeMove()).thenReturn(Moves.CHEAT);
        when(player2.makeMove()).thenReturn(Moves.COOPERATE);
        PrintStream out = mock(PrintStream.class);
        Score score = new Score(34, 22);
        when(machine.calculateScore(Moves.CHEAT, Moves.COOPERATE)).thenReturn(score);


        new Game(player1, player2, machine, rounds, out).start();


        verify(player1, times(rounds)).makeMove();
        verify(player2, times(rounds)).makeMove();

        verify(machine, times(2)).calculateScore(Moves.CHEAT, Moves.COOPERATE);

        verify(out).println(score.getPlayer1Score() + " " + score.getPlayer2Score());
        verify(out).println(2 * score.getPlayer1Score() + " " + 2 * score.getPlayer2Score());

    }

}
