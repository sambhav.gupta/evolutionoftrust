package com.app.bootcamp;

import org.junit.Assert;
import org.junit.Test;

public class CheatPlayerMoveBehaviorTest {
    @Test
    public void shouldReturnCheatMove() {

        //IPlayerReader reader = () -> "CHEAT";

        Player player1 = new Player(new CheatPlayerMoveBehavior(), "Player1");
        Moves player1Move = player1.makeMove();
        Assert.assertEquals(Moves.CHEAT,player1Move);
    }
}
