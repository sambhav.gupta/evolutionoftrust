package com.app.bootcamp;

import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class ConsolePlayerMoveBehaviorTest {

    @Test
    public void shouldReturnNonCheatMoveByPlayer1() {
        //IPlayerReader reader = () -> new Scanner(System.in).nextLine();

        IPlayerReader reader = () -> "COOPERATE";

        Player player1 = new Player(new ConsolePlayerMoveBehavior(reader), "Player1");
        Moves player1Move = player1.makeMove();
        Assert.assertEquals(Moves.COOPERATE,player1Move);
    }


    @Test
    public void shouldReturnCheatMoveByPlayer1() {
        //IPlayerReader reader = () -> new Scanner(System.in).nextLine();

        IPlayerReader reader = () -> "CHEAT";

        Player player1 = new Player(new ConsolePlayerMoveBehavior(reader), "Player1");
        Moves player1Move = player1.makeMove();
        Assert.assertEquals(Moves.CHEAT,player1Move);
    }


    @Test
    public void shouldHandleErrorInputPlayer1(){
        Scanner sc = new Scanner("RANDOM\nCHEAT");
        IPlayerReader reader = () -> sc.nextLine();

        Player player1 = new Player(new ConsolePlayerMoveBehavior(reader), "Player1");
        Moves player1Move = player1.makeMove();
        Assert.assertEquals(Moves.CHEAT,player1Move);

    }
}
