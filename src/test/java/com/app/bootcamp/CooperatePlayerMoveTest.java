package com.app.bootcamp;

import org.junit.Assert;
import org.junit.Test;

public class CooperatePlayerMoveTest {
    @Test
    public void shouldReturnCooperateMove() {

        Player player1 = new Player(new CooperatePlayerMoveBehavior(), "Player1");
        Moves player1Move = player1.makeMove();
        Assert.assertEquals(Moves.COOPERATE,player1Move);
    }
}
