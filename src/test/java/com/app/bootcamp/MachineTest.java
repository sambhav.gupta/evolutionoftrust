package com.app.bootcamp;

import org.junit.Assert;
import org.junit.Test;

public class MachineTest {

    @Test
    public void shouldCalculateIndividualScoresWhenBothPlayersCooperate(){
        Score score = new Score(2,2);
        int expectedPlayer1Score= 2;
        int expectedPlayer2Score= 2;
        int actualPlayer1Score = score.getPlayer1Score();
        int actualPlayer2Score = score.getPlayer2Score();
        Assert.assertEquals(expectedPlayer1Score,actualPlayer1Score);
        Assert.assertEquals(expectedPlayer2Score,actualPlayer2Score);

    }

    @Test
    public void shouldCalculateScoreWhenBothPlayersCooperate(){
        int expectedPlayer1Score= 2;
        int expectedPlayer2Score= 2;
        Machine machine= new Machine();
        Score actualScore= machine.calculateScore(Moves.COOPERATE,Moves.COOPERATE);
        Assert.assertEquals(expectedPlayer1Score,actualScore.getPlayer1Score());
        Assert.assertEquals(expectedPlayer2Score,actualScore.getPlayer2Score());

    }


    @Test
    public void shouldCalculateScoreWhenBothPlayer1OnlyCooperates(){
        int expectedPlayer1Score= -1;
        int expectedPlayer2Score= 3;
        Machine machine= new Machine();
        Score actualScore= machine.calculateScore(Moves.COOPERATE,Moves.CHEAT);
        Assert.assertEquals(expectedPlayer1Score,actualScore.getPlayer1Score());
        Assert.assertEquals(expectedPlayer2Score,actualScore.getPlayer2Score());

    }


    @Test
    public void shouldCalculateScoreWhenBothPlayer2OnlyCooperates(){
        int expectedPlayer1Score= 3;
        int expectedPlayer2Score= -1;
        Machine machine= new Machine();
        Score actualScore= machine.calculateScore(Moves.CHEAT,Moves.COOPERATE);
        Assert.assertEquals(expectedPlayer1Score,actualScore.getPlayer1Score());
        Assert.assertEquals(expectedPlayer2Score,actualScore.getPlayer2Score());

    }
    @Test
    public void shouldCalculateScoreWhenBothPlayersDoNotCooperate(){
        int expectedPlayer1Score= 0;
        int expectedPlayer2Score= 0;
        Machine machine= new Machine();
        Score actualScore= machine.calculateScore(Moves.CHEAT,Moves.CHEAT);
        Assert.assertEquals(expectedPlayer1Score,actualScore.getPlayer1Score());
        Assert.assertEquals(expectedPlayer2Score,actualScore.getPlayer2Score());

    }


}
