package com.app.bootcamp;

import org.junit.Test;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CopyCatTestBehavior {

    @Test
    public void shouldReturnMoveOfAlwaysCooperatePlayer() {
        Player player = new Player(() -> Moves.COOPERATE, "Player1");
        Moves previouslyPlayed = player.makeMove();
        Player copyCatPlayer = new Player(new CopyCatBehavior(player.getName()), "CopyCatPlayer");

        assertEquals(previouslyPlayed, copyCatPlayer.makeMove());
    }

    @Test
    public void shouldReturnCheatOfAlwaysCheatPlayer() {
        Player player = new Player(() -> Moves.CHEAT, "Player1");

        Machine machine = new Machine();

        CopyCatBehavior copyCatBehavior = new CopyCatBehavior(player.getName());
        Player copyCatPlayer = new Player(copyCatBehavior, "CopyCatPlayer");
        PrintStream out = mock(PrintStream.class);
        Game game = new Game(player, copyCatPlayer, machine, 5, out);
        game.addObserver(copyCatBehavior);

        game.start();

        verify(out).println( "3 -1");
        verify(out).println( "3 -1");
        verify(out).println( "3 -1");
        verify(out).println( "3 -1");
        verify(out).println( "3 -1");
    }

    @Test
    public void shouldReturnCheatOfAlwaysCopyCatPlayer() {
        CopyCatBehavior copyCatBehavior = new CopyCatBehavior("CopyCatPlayer");
        Player player = new Player(copyCatBehavior, "Player1");

        Machine machine = new Machine();

        CopyCatBehavior copyCatBehavior2 = new CopyCatBehavior(player.getName());
        Player copyCatPlayer = new Player(copyCatBehavior2, "CopyCatPlayer");
        PrintStream out = mock(PrintStream.class);
        Game game = new Game(player, copyCatPlayer, machine, 5, out);
        game.addObserver(copyCatBehavior);

        game.start();

        verify(out).println( "2 2");
        verify(out).println( "4 4");
        verify(out).println( "6 6");
        verify(out).println( "8 8");
        verify(out).println( "10 10");
    }
}
