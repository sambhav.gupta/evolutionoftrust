package com.app.bootcamp;

public class ConsolePlayerMoveBehavior implements PlayerMoveBehavior {
    private IPlayerReader reader;

    public ConsolePlayerMoveBehavior(IPlayerReader reader) {
        this.reader = reader;
    }


    @Override
    public Moves makeMove() {

        Moves output = Moves.NA;
        while (!output.equals(Moves.CHEAT) && !output.equals(Moves.COOPERATE)) {
            String input = reader.read();
            output = parseInput(input);
        }
        return output;
    }

    private Moves parseInput(String move) {
        if(move.equals("COOPERATE"))
            return Moves.COOPERATE;
        else if(move.equals("CHEAT"))
            return Moves.CHEAT;
        else
            return Moves.NA;
    }
}