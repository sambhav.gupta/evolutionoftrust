package com.app.bootcamp;

public interface PlayerMoveBehavior {
    Moves makeMove();
}
