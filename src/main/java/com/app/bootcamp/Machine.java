package com.app.bootcamp;


enum Moves{

    COOPERATE, CHEAT, NA
}
public class Machine {

    public Score calculateScore(Moves player1Input,Moves player2Input) {
     if(player1Input.equals(Moves.COOPERATE) && player2Input.equals(Moves.COOPERATE))
        return new Score(2,2);
     else if(player1Input.equals(Moves.COOPERATE) && player2Input.equals(Moves.CHEAT)){
         return new Score(-1,3);
     }
     else if(player1Input.equals(Moves.CHEAT) && player2Input.equals(Moves.COOPERATE)){
         return new Score(3,-1);
     }

     return new Score(0,0);
    }
}
