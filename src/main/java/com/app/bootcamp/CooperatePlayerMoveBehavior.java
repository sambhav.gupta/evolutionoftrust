package com.app.bootcamp;

public class CooperatePlayerMoveBehavior implements PlayerMoveBehavior {

    @Override
    public Moves makeMove() {

        return Moves.COOPERATE;
    }
}
