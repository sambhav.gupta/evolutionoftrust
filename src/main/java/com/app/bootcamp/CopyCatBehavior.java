package com.app.bootcamp;

import java.util.Observable;
import java.util.Observer;

public class CopyCatBehavior implements PlayerMoveBehavior, Observer {


    private final String opponentName;
    private Moves nextMove = Moves.COOPERATE;

    public CopyCatBehavior(String opponentName) {
        this.opponentName = opponentName;
    }

    @Override
    public Moves makeMove() {
        return nextMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        Notification notification = (Notification) arg;
        String opponentName = notification.getPlayerName();
        if (this.opponentName.equals(opponentName)) {
            nextMove = notification.getPlayerMove();
        }
    }
}
