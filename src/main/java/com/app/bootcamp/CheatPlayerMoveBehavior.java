package com.app.bootcamp;

public class CheatPlayerMoveBehavior implements PlayerMoveBehavior {

    @Override
    public Moves makeMove() {

        return Moves.CHEAT;
    }
}
