package com.app.bootcamp;

import java.util.Observable;
import java.util.Observer;

public class GrudgeBehavior implements PlayerMoveBehavior, Observer {

    private final String opponentName;
    private Moves move = Moves.COOPERATE;

    public GrudgeBehavior(String opponentName) {
        this.opponentName = opponentName;
    }

    @Override
    public Moves makeMove() {
        return move;
    }

    @Override
    public void update(Observable o, Object arg) {
        Notification notification = (Notification) arg;
        if (opponentName.equals(notification.getPlayerName()) && Moves.CHEAT.equals(notification.getPlayerMove())) {
            move = Moves.CHEAT;
        }
    }
}
