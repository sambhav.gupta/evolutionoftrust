package com.app.bootcamp;

public class Player {

    private final PlayerMoveBehavior playerMoveBehavior;
    private String name;

    public Player(PlayerMoveBehavior playerBehavior, String name) {
        this.playerMoveBehavior = playerBehavior;
        this.name = name;
    }


    public Moves makeMove() {
        return playerMoveBehavior.makeMove();
    }


    public String getName() {
        return this.name;
    }
}
