package com.app.bootcamp;

import java.io.PrintStream;
import java.util.Observable;

public class Game extends Observable {
    private Player player1, player2;
    private Machine machine;
    private int rounds;
    private PrintStream out;

    public Game(Player player1, Player player2, Machine machine, int rounds, PrintStream out) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.rounds = rounds;
        this.out = out;
    }


    public void start() {
        int i = 0;
        int score1 = 0;
        int score2 = 0;
        while (i < rounds) {
            Moves player1Move = player1.makeMove();
            Moves player2Move = player2.makeMove();
            setChanged();
            notifyObservers(new Notification(player2.getName(), player2Move));
            notifyObservers(new Notification(player1.getName(), player1Move));
            Score score = machine.calculateScore(player1Move, player2Move);
            score1 += score.getPlayer1Score();
            score2 += score.getPlayer2Score();
            out.println(score1 + " " + score2);
            i++;
        }

    }
}
