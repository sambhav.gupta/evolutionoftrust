package com.app.bootcamp;

public class Notification {
    private final String playerName;
    private final Moves playerMove;

    public Notification(String playerName, Moves playerMove) {
        this.playerName = playerName;
        this.playerMove = playerMove;
    }

    public String getPlayerName() {
        return playerName;
    }

    public Moves getPlayerMove() {
        return playerMove;
    }
}
