package com.app.bootcamp;

@FunctionalInterface
public interface IPlayerReader {

    String read();
}
